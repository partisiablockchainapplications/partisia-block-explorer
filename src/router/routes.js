
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'transactions', component: () => import('pages/Transactions.vue') },
      { path: 'transaction/:hash', component: () => import('pages/Transaction.vue') },
      { path: 'validators', component: () => import('pages/Validators.vue') },
      { path: 'blocks', component: () => import('pages/Blocks.vue') },
      { path: 'node-register', component: () => import('pages/NodeRegister.vue') },
      { path: 'deploy-contract', component: () => import('pages/ContractDeployWasm.vue') },
      { path: 'block/:hash', component: () => import('pages/Block.vue') },
      { path: 'address/:address', component: () => import('pages/Address.vue') },
      { path: 'contract/:address', component: () => import('pages/Contract.vue') },
      { path: 'community', component: () => import('pages/CommunityStaking.vue') },
      { path: 'contract-abi-decoder', component: () => import('pages/ContractAbiDecoder.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
