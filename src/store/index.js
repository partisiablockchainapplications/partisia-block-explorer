import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import { defineComponent, ref, computed, getCurrentInstance } from 'vue'
const app = getCurrentInstance()

// import example from './module-example'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

const defaultStateChain = () => {
  return {
    loadError: null,
    status: [],
    transactions: [],
    blocks: [],
    validators: { committees: [] },
    onWalletConnect: 0,
  }
}

export default store(function (/* { ssrContext } */) {
  const keyStorage = 'vuex'
  const persistedState = createPersistedState({
    key: keyStorage,
    paths: ['sdkClint'],
  })

  const Store = createStore({
    plugins: [persistedState],
    modules: {
      sdkClint: {
        state: {
          sdk: null,
        },
        mutations: {
          CLEAR: (state) => {
            state.sdk = null
          },
          CONNECT: (state, payload) => {
            state.sdk = payload
          },
        },
        actions: {
          sdkClear: (context) => {
            context.commit('CLEAR')
          },
          sdkConnect: (context, payload) => {
            context.commit('CONNECT', payload)
          }
        },
        getters: {
          sdkClient: (state, getters) => {
            return state.sdk
          },
        },
      },
      chain: {
        state: defaultStateChain(),
        mutations: {
          RESET_STATE_CHAIN: (state) => {
            Object.assign(state, defaultStateChain())
          },
          LOAD_ERROR: (state, payload) => {
            state.loadError = payload
          },
          SET_STATUS: (state, payload) => {
            state.status = payload
          },
          SET_TRANSACTIONS: (state, payload) => {
            state.transactions = payload
          },
          SET_BLOCKS: (state, payload) => {
            state.blocks = payload
          },
          SET_VALIDATORS: (state, payload) => {
            state.validators = payload
          },
          NEW_BLOCK: (state, payload) => {
            const blocks = state.blocks
            blocks.unshift(payload)
            while (blocks.length > process.env.CACHE_PAGINATION) {
              blocks.pop()
            }
            state.blocks = [...blocks]

            // state.blocks = JSON.parse(JSON.stringify(state.blocks))
            // console.log('state.blocks', state.blocks)
            let idxStatus = state.status.findIndex(s => s.shard_id == payload.shard_id)
            if (idxStatus !== -1) {
              const { block_level, block_hash, block_hash_previous, block_timestamp, transaction_count } = payload
              state.status[idxStatus] = {
                ...state.status[idxStatus],
                block_level,
                block_hash,
                block_hash_previous,
                block_timestamp,
                transaction_count: state.status[idxStatus].transaction_count += transaction_count,
              }
            }
          },
          NEW_TRANSACTION: (state, payload) => {
          },
          SET_WALLET_CONNECT: (state, payload) => {
            state.onWalletConnect = payload
          },
        },
        actions: {
          resetCart: ({ commit }) => {
            commit('RESET_STATE_CHAIN')
          },
          loadError: ({ commit }, payload) => {
            commit('LOAD_ERROR', payload)
          },
          setStatus: (context, payload) => {
            context.commit('SET_STATUS', payload)
          },
          setTransactions: (context, payload) => {
            context.commit('SET_TRANSACTIONS', payload)
          },
          setBlocks: (context, payload) => {
            context.commit('SET_BLOCKS', payload)
          },
          setValidators: (context, payload) => {
            context.commit('SET_VALIDATORS', payload)
          },
          wsMessage: (context, { data_type, payload }) => {
            if (data_type === 'Block') {
              context.commit('NEW_BLOCK', payload)
            } else if (data_type === 'Transaction') {
              context.commit('NEW_TRANSACTION', payload)
            }
          },
          triggerWalletConnect: (context) => {
            context.commit('SET_WALLET_CONNECT', Date.now())
          },
        },
        getters: {
          onWalletConnect: (state) => {
            return state.onWalletConnect
          },
          loadError: (state) => {
            return state.loadError
          },
          transactionsCount: (state, getters) => {
            return state.status.map(s => s.transaction_count).reduce((pv, cv, idx) => pv + cv, 0)
          },
          status: (state, getters) => {
            return state.status
          },
          transactions: (state, getters) => {
            return state.transactions
          },
          blocks: (state, getters) => {
            return state.blocks
          },
          validators: (state, getters) => {
            return state.validators
          },
          isLoggedIn: (state, getters) => {
            return !!getters.sdkClient?.connection?.account
          },
        },
      }
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING
  })

  return Store
})
