import { exportFile } from 'quasar'

const exportData = (content = [], name = 'file.csv') => {

    const status = exportFile(
        name,
        content,
        {
            mimeType: 'text/csv',
        }
    )
    return status
}


export { exportData }