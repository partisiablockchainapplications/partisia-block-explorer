import { boot } from 'quasar/wrappers'
import axios from 'axios'
import { partisiaCrypto } from 'partisia-crypto'
import { fromWei } from 'web3-utils'
import { PartisiaAccount } from 'partisia-rpc'
import { Big } from 'big.js'
import { ProtoBuf } from 'partisia-websocket-protobufs'
import { AbiParser, FnKinds, JsonRpcConverter, RpcReader } from '@partisiablockchain/abi-client-ts'
import assert from 'assert'
import tocase from 'to-case'

const mapShardNames = [{ id: 99, name: 'gov' }]

// the id is the index from global coins plus 1
export const mapCoinNames = process.env.TESTNET ? [
    { id: 1, name: 'ETH GOERLI', decimals: 18 },
    { id: 2, name: 'TEST COIN', decimals: 0 },
] : [
    { id: 1, name: 'ETH', decimals: 18 },
    { id: 2, name: 'USDC', decimals: 6 },
    { id: 3, name: 'BNB', decimals: 18 },
]

// export const BYOC_Coin_Decimals = Object.freeze({
//     MPC_ROPSTEN: 18,
//     ETH: 18,
//     POLYGON_USDC: 6,
// })
const api = axios.create({ baseURL: process.env.API_INDEXER_URL })
const protoBuf = new ProtoBuf()

export default boot(async ({ app, store, router }) => {
    app.config.globalProperties.$axios = axios
    app.config.globalProperties.$api = api
    app.config.globalProperties.$router = router
    app.config.globalProperties.$mapShardNames = mapShardNames
    app.config.globalProperties.$testnet = process.env.TESTNET
    app.config.globalProperties.$dev = process.env.DEV

    try {
        const status = await getStatus()
        store.dispatch('setStatus', status)
    } catch (error) {
        console.error(error.message)
        store.dispatch('loadError', error.message)
    }

    // init the wasm from decodeProtoBuffers
    await protoBuf.init() // set up the websocket
    ws_listen(store)
})

const ws_listen = (store) => {
    var ws = new WebSocket(process.env.WEB_SOCKET_URL);
    ws.binaryType = 'arraybuffer';

    ws.onclose = (event) => {
        console.log('close Reconnect will be attempted in 1 second.', { code: event.code, reason: event.reason });
        setTimeout(() => {
            ws_listen(store);
        }, 1000);
    };
    ws.onerror = (err) => {
        console.error('Socket encountered error: ', err.message, 'Closing socket');
        ws.close();
    };
    ws.onopen = () => {
        const message = {
            data: [
                { subscribe_type: 'Block', },
                // { subscribe_type: 'Transaction', },
            ],
        };
        console.log('subscribing:', message.data.map((m) => m.subscribe_type));
        // let b = Buffer.from(JSON.stringify(message), 'utf8');
        // var ab = new Uint8Array(b).buffer
        // ws.send(ab);
        // console.log('ab', ab.buffer)

        ws.send(JSON.stringify(message));
    };

    ws.onmessage = async (ev) => {
        if (typeof ev.data === 'object') {
            try {
                // if (process.env.DEV) console.log('ev.data', Buffer.from(ev.data).toString('hex'))

                const decode = protoBuf.decodeProtoBuffers(Buffer.from(ev.data))
                // if (process.env.DEV) console.log('ws.onmessage', JSON.stringify(decode, null, 2));
                await store.dispatch('wsMessage', decode)
            } catch (err) {
                console.error('ws.onmessage err', err.message)
            }
        } else if (typeof ev.data === 'string') {
            // This is an error
            console.log(ev.data);
        }
    };
}

const getShardName = (id) => {
    return mapShardNames.find((s) => s.id == id)?.name || id
}
// const snakeToProperCase = (str) => {
//     var str = str.toLowerCase().replace(/([-_][a-z])/g, (group) => group.toUpperCase().replace('-', '').replace('_', ' '))
//     return `${str.charAt(0).toUpperCase()}${str.slice(1)}`
// }
const getValidators = async () => {
    // const { data } = await axios.post(process.env.API_VALIDATORS_URL, { path: [] }, { responseType: 'json', headers: { 'Content-Type': 'application/json' } })
    const { data } = await axios.get(process.env.API_VALIDATORS_URL)
    return data
}
const getValidatorsAll = async () => {
    const { data } = await axios.post(process.env.API_VALIDATORS_URL_ALL, { path: [] }, { responseType: 'json', headers: { 'Content-Type': 'application/json' } })
    return data
}

const getStatus = async () => {
    const { data } = await api.get('/status')
    return JSON.parse(JSON.stringify(data))
}

const SYSTEM_CONTRACTS = partisiaCrypto.structs.SYSTEM_CONTRACTS

// this comes from:
// impl ReadWriteRPC for Address
// rust-contract-sdk/pbc_contract_common/src/address.rs
const addressType = (address) => {
    const byte = parseInt(address.substr(0, 2), 16)
    switch (byte) {
        case 0:
            return 'Address'
        case 1:
            return 'System Contract'
        case 2:
            return 'Public Contract'
        case 3:
            return 'ZK Contract'
        default:
            // Really this is unknown but display contract
            return 'Contract'
    }
}

const getInvocationType = (headerContract) => {
    return SYSTEM_CONTRACTS.find(c => c.contract === headerContract)?.name || addressType(headerContract)
}
const mapTrxData = (data) => {
    for (const d of data) {
        d.invocation_type = getInvocationType(d.header_contract)
        d.to = d.header_contract
    }
    return data
}
const reMapByoc = (data) => {
    // group byoc by id
    const aryIds = data.map((x) => x.id).filter((val, idx, ary) => ary.indexOf(val) === idx)
    const ary = aryIds.map((id) => {
        const row = data.find((v) => v.id === id)
        const { coin_id, deposit_type, nonce, address, amount, eth_transaction_hash } = row
        const coinProperties = mapCoinNames.find((s) => s.id == coin_id)
        const amountConvert = coinProperties ? `${new Big(amount).times(`1e-${coinProperties.decimals}`)} ${coinProperties.name}` : amount
        return {
            id,
            coin_id: coin_id,
            coin_name: coinProperties?.name || coin_id,
            deposit_type: (deposit_type === 0 ? 'Deposit' : 'Withdraw'),
            nonce,
            address,
            amount,
            amount_convert: amountConvert,
            eth_transaction_hash,
            inner: data
                .filter((val) => val.id === id)
                .map((v, idx) => {
                    return {
                        id: `${v.id}.${idx}`,
                        shard_id: v.shard_id,
                        transaction_hash: v.transaction_hash,
                        block_hash: v.block_hash,
                        block_timestamp: v.block_timestamp,
                        block_level: v.block_level,
                        inner_cost: v.inner_cost,
                    }
                }),
        }
    })
    // console.log('ary',JSON.stringify(ary,null,2))
    return ary
}
const mapTrxDataErc20 = (data, abi, decimals) => {
    const abiFile = new AbiParser(Buffer.from(abi, 'hex')).parseAbi()
    for (const d of data) {
        d.invocation_type = getInvocationType(d.header_contract)
        d.to = d.header_contract
        if (!d.has_error && parseInt(d.header_contract.substr(0, 2), 16) === 2) {
            try {
                const payload = Buffer.from(d.payload_data, 'hex')
                const rpc = new RpcReader(payload, abiFile, FnKinds.action).readRpc()
                if (rpc.fnAbi.name === 'transfer') {
                    const arg = rpc.arguments
                    const to = arg[0]
                    const amount = arg[1]
                    d.to = to.addressValue().value.toString('hex')
                    d.amount = convertDec(amount.asBN().toString(), { decimals })
                }
            } catch (error) {
                console.error(error.message)

            }
        }
    }
    return data
}
const mapTrxDataErc721 = (data, abi, decimals) => {
    const abiFile = new AbiParser(Buffer.from(abi, 'hex')).parseAbi()
    for (const d of data) {
        d.invocation_type = getInvocationType(d.header_contract)
        d.to = d.header_contract
    }
    return data
}

const getAbi = (headerContract) => {
    return SYSTEM_CONTRACTS.find(c => c.contract === headerContract)?.abi
}

const deserializePayloadData = (payloadData, contract) => {
    const aryAbi = getAbi(contract)
    if (!aryAbi) { return null }

    const enumInvocation = payloadData[0]
    const abi = aryAbi[Number(enumInvocation)]
    if (!abi) { return null }
    const objPayload = partisiaCrypto.structs.deserializeFromBuffer(payloadData, ...abi)
    return objPayload

}
const postProcessPayloadData = (objPayload, contract, data) => {
    switch (contract) {

        case '0197a0e238e924025bad144aa0c4913e46308f9a4d':
            objPayload.deployAddress = `02${data.transaction_hash.substr(24)}`
            // objPayload.init = {x:'y'}
            break;
        case '045dbd4c13df987d7fb4450e54bcd94b34a80f2351':
            objPayload.depositType = objPayload.depositType === 0 ? 'Deposit' : 'Withdraw'
            objPayload.amount = `${fromWei(objPayload.amount)} ETH`
            break;
        case '043b1822925da011657f9ab3d6ff02cf1e0bfe0146':
            if (objPayload.hasOwnProperty('amount')) {
                objPayload.amount = `${fromWei(objPayload.amount)} ETH`
            }
            break;
    }
}
const rpcConfig = JSON.parse(process.env.RPC_ACCOUNT_CONFIG)
if (process.env.DEV) console.log('rpcConfig', JSON.stringify(rpcConfig, null, 2))
const rpcAccount = PartisiaAccount(rpcConfig)
const convertDec = (amt, opts = {}) => {
    var { conversion = 1, decimals = 0, convertFromDecimals = true, removeTrailingZeros = true } = opts
    if (convertFromDecimals) conversion = `1e${decimals}`

    // seems like this doesnt need to be adjusted for the 4 decimals places
    // https://stackoverflow.com/questions/721304/insert-commas-into-number-string
    let amtStr = new Big(amt).div(conversion).toFixed(Number(decimals))
    if (amtStr.indexOf('.') !== -1) {
        while (amtStr.slice(-1) === '0' && removeTrailingZeros) {
            amtStr = amtStr.slice(0, -1)
        }
        if (amtStr.slice(-1) === '.') {
            amtStr = amtStr.slice(0, -1)
        }
    }
    if (amtStr.indexOf('.') !== -1) {
        return amtStr.replace(/\d{1,3}(?=(\d{3})+(?=\.))/g, "$&,")
    } else {
        return amtStr.replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, "$&,")
    }
}

// const convertDec = (amt, conversion = 10000) => {
//     // seems like this doesnt need to be adjusted for the 4 decimals places
//     // https://stackoverflow.com/questions/721304/insert-commas-into-number-string
//     return new Big(amt).div(conversion).toFixed(4).replace(/(?<!\.)\d{1,3}(?=(\d{3})+(?!\d))/g, "$&,");
// }
function truncateWithEllipses(text, max) { return text.substr(0, max - 1) + (text.length > max ? '...' : ''); }
export const sleep = (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms))
}
// https://stackoverflow.com/a/49408568/7959616
export const flattenObject = (obj) => {
    const result = {}
    Object.keys(obj).forEach((key) => {
        const value = obj[key]
        if (typeof value === 'object') {
            const flattened = flattenObject(value)
            Object.keys(flattened).forEach((subKey) => {
                result[`${key}_${subKey}`] = flattened[subKey]
            })
        } else {
            result[key] = value
        }
    })
    return result
}

export const deserializeRpcFn = (abi, payload, isInit) => {
    // deserialize the inner transaction payload
    const abiFileContract = new AbiParser(abi).parseAbi()
    const rpc = new RpcReader(payload, abiFileContract, isInit ? FnKinds.init : FnKinds.action).readRpc()

    return rpc
}
export {
    SYSTEM_CONTRACTS,
    api,
    getShardName, getValidators, getValidatorsAll, getStatus, mapTrxData, reMapByoc, mapTrxDataErc20, mapTrxDataErc721, getInvocationType, deserializePayloadData, postProcessPayloadData,
    rpcAccount, convertDec, truncateWithEllipses
}
