# Partisia Block Explorer (partisia-block-explorer)

Block Explorer for the Partisia Blockchain

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
npm run dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
npm run build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).


### fonts
> http://google-webfonts-helper.herokuapp.com/fonts